package com.gaurdianaq.pantryorganizer

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.gaurdianaq.pantryorganizer.adapter.FoodCategorySpinnerAdapter
import com.gaurdianaq.pantryorganizer.database.*
import com.gaurdianaq.pantryorganizer.viewmodel.FoodbyLocViewModel
import kotlinx.android.synthetic.main.activity_create_foodbyloc.*
import kotlinx.android.synthetic.main.content_create_cat.*
import java.lang.Exception

class CreateFoodByLoc : AppCompatActivity() {

    private lateinit var editName: EditText
    private lateinit var editQty: EditText
    private lateinit var editFoodCat: Spinner
    private lateinit var categories: Array<FoodCategory>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_foodbyloc)
        setSupportActionBar(createFoodToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        categories = intent.extras?.getSerializable(categoryFlag) as Array<FoodCategory>
        editName = findViewById(R.id.editFoodName)
        editQty = findViewById(R.id.editQty)
        editFoodCat = findViewById(R.id.catpicker)
        val categoryAdapter = FoodCategorySpinnerAdapter(this, android.R.layout.simple_spinner_item, categories.toList())
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        editFoodCat.adapter = categoryAdapter
    }

    fun cancel(view: View) {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    fun submit(view: View) {
        if (TextUtils.isEmpty(editName.text) && (TextUtils.isEmpty(editQty.text)))
        {
            val warningToast = Toast.makeText(this, "Text fields can't be empty!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else if (TextUtils.isEmpty(editName.text))
        {
            val warningToast = Toast.makeText(this, "Name field is empty! Not allowed!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else if (TextUtils.isEmpty(editQty.text))
        {
            val warningToast = Toast.makeText(this, "Qty field is empty! Not allowed!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else
        {
            val replyIntent = Intent()
            val location = intent.extras?.getSerializable(locationFlag) as Location
            val food = Food(0, (editFoodCat.selectedItem as FoodCategory).name, location.name, editName.text.toString(), editQty.text.toString().toInt())
            replyIntent.putExtra(foodFlag, food)
            setResult(Activity.RESULT_OK, replyIntent)
            finish()
        }
    }

}
