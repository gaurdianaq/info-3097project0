package com.gaurdianaq.pantryorganizer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gaurdianaq.pantryorganizer.adapter.FoodListAdapter
import com.gaurdianaq.pantryorganizer.database.Food
import com.gaurdianaq.pantryorganizer.database.FoodCategory
import com.gaurdianaq.pantryorganizer.database.Location
import com.gaurdianaq.pantryorganizer.viewmodel.FoodbyCatViewModel
import com.gaurdianaq.pantryorganizer.viewmodel.FoodbyLocViewModel

import kotlinx.android.synthetic.main.activity_recycle_foodby_cat.*

class RecycleFoodbyCat : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var category: FoodCategory
    private lateinit var locations:List<Location>
    private lateinit var foodViewModel:FoodbyCatViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycle_foodby_cat)
        setSupportActionBar(recycleFoodToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        foodViewModel = ViewModelProvider(this).get(FoodbyCatViewModel::class.java)
        category = intent.extras?.getSerializable(categoryFlag) as FoodCategory
        foodViewModel.reloadData(category)
        recyclerView = findViewById(R.id.recyclebycat)
        val adapter = FoodListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        foodViewModel.food.observe(this, Observer { food ->
            food?.let { adapter.setFood(it) }
        })

        foodViewModel.locations.observe(this, Observer { locations ->
            this.locations = locations
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_foodbycat, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.createFood -> {
                val myIntent = Intent(this, CreateFoodByCat::class.java)
                myIntent.putExtra(locationFlag, locations.toTypedArray())
                myIntent.putExtra(categoryFlag, category)
                startActivityForResult(myIntent, insertRequestCode)
                true
            }
            R.id.about -> {
                val myIntent = Intent(this, About::class.java)
                startActivity(myIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == insertRequestCode && resultCode == Activity.RESULT_OK)
        {
            lateinit var food: Food
            data?.extras?.let {
                food = it.getSerializable(foodFlag) as Food
                foodViewModel.insert(food)
            }
            val okToast = Toast.makeText(this, "Added ${food.name}!", Toast.LENGTH_LONG)
            okToast.show()
        }
        else if(requestCode == insertRequestCode && resultCode == Activity.RESULT_CANCELED)
        {
            val cancelToast = Toast.makeText(this, "Cancelled food add!", Toast.LENGTH_LONG)
            cancelToast.show()
        }
    }

}
