package com.gaurdianaq.pantryorganizer

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.gaurdianaq.pantryorganizer.database.FoodCategory
import com.gaurdianaq.pantryorganizer.database.Location
import com.gaurdianaq.pantryorganizer.database.PantryDAO
import com.gaurdianaq.pantryorganizer.database.PantryDatabase
import kotlinx.android.synthetic.main.activity_create_cat.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CreateCat : AppCompatActivity() {
    lateinit var editCatName: EditText
    lateinit var editCatDesc: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_cat)
        setSupportActionBar(createCatToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        editCatName = findViewById(R.id.editCatName)
        editCatDesc = findViewById(R.id.editCatDesc)
    }

    fun cancel(view: View) {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    fun submit(view: View) {
        if (TextUtils.isEmpty(editCatName.text) && TextUtils.isEmpty(editCatDesc.text))
        {
            val warningToast = Toast.makeText(this, "Name and Description are empty! Not allowed!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else if (TextUtils.isEmpty(editCatName.text))
        {
            val warningToast = Toast.makeText(this, "Name is empty! Not allowed!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else if (TextUtils.isEmpty(editCatDesc.text))
        {
            val warningToast = Toast.makeText(this, "Description is empty! Not allowed!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else
        {
            val replyIntent = Intent()
            val category = FoodCategory(editCatName.text.toString(), editCatDesc.text.toString())
            replyIntent.putExtra(categoryFlag, category)
            setResult(Activity.RESULT_OK, replyIntent)
            finish()
        }
    }
}
