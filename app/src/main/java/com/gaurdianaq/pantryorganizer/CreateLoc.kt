package com.gaurdianaq.pantryorganizer

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import com.gaurdianaq.pantryorganizer.database.*
import kotlinx.android.synthetic.main.activity_create_loc.*

class CreateLoc : AppCompatActivity() {
    lateinit var dao: PantryDAO
    lateinit var spinner: Spinner
    lateinit var locName: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_loc)
        setSupportActionBar(createLocToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        dao = PantryDatabase.getDatabase(this).dao()
        spinner = findViewById(R.id.locCat)
        locName = findViewById(R.id.editLocName)
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.locationCat,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

    }

    fun cancel(view: View) {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    fun submit(view: View) {
        if (TextUtils.isEmpty(locName.text))
        {
            val warningToast = Toast.makeText(this, "Name can't be empty!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else
        {
            val replyIntent = Intent()
            val location = Location(locName.text.toString(), spinner.selectedItem.toString())
            replyIntent.putExtra(locationFlag, location)
            setResult(Activity.RESULT_OK, replyIntent)
            finish()
        }

    }
}
