package com.gaurdianaq.pantryorganizer.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.gaurdianaq.pantryorganizer.database.*
import kotlinx.coroutines.launch
import java.lang.Exception

class FoodbyCatViewModel(application: Application) : AndroidViewModel(application)
{
    private val dao: PantryDAO
    var food: LiveData<List<Food>>
    val locations: LiveData<List<Location>>

    init {
        dao = PantryDatabase.getDatabase(application).dao()
        locations = dao.loadLocation()
        food = dao.loadAllFood()
    }

    //make sure this is called before accessing food, I'll see if there is a way to add this back into the constructor
    fun reloadData(category: FoodCategory)
    {
        food = dao.loadFoodByFoodCategory(category.name)
    }

    fun reloadData()
    {
        food = dao.loadAllFood()
    }


    fun insert(food: Food) = viewModelScope.launch {
        dao.insertFood(food)
    }
}