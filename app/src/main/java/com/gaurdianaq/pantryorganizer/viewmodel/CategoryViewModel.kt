package com.gaurdianaq.pantryorganizer.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.gaurdianaq.pantryorganizer.database.FoodCategory
import com.gaurdianaq.pantryorganizer.database.Location
import com.gaurdianaq.pantryorganizer.database.PantryDAO
import com.gaurdianaq.pantryorganizer.database.PantryDatabase
import kotlinx.coroutines.launch

class CategoryViewModel(application: Application) : AndroidViewModel(application)
{
    private val dao: PantryDAO
    val categories: LiveData<List<FoodCategory>>

    init {
        dao = PantryDatabase.getDatabase(application).dao()
        categories = dao.loadFoodCategory()
    }

    fun insert(category:FoodCategory) = viewModelScope.launch {
        dao.insertFoodCategory(category)
    }
}