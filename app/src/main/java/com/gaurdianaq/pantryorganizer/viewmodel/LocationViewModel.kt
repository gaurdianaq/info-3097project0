package com.gaurdianaq.pantryorganizer.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.gaurdianaq.pantryorganizer.database.*
import kotlinx.coroutines.launch

class LocationViewModel(application: Application) : AndroidViewModel(application)
{
    private val dao: PantryDAO
    val locations: LiveData<List<Location>>

    init {
        dao = PantryDatabase.getDatabase(application).dao()
        locations = dao.loadLocation()
    }

    fun insert(location: Location) = viewModelScope.launch {
        dao.insertLocation(location)
    }

}