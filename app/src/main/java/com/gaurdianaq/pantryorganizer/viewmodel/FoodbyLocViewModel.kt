package com.gaurdianaq.pantryorganizer.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.gaurdianaq.pantryorganizer.database.*
import kotlinx.coroutines.launch

class FoodbyLocViewModel(application: Application) : AndroidViewModel(application)
{
    private val dao: PantryDAO
    var food: LiveData<List<Food>>
    val categories: LiveData<List<FoodCategory>>

    init {
        dao = PantryDatabase.getDatabase(application).dao()
        categories = dao.loadFoodCategory()
        food = dao.loadAllFood()
    }

    fun reloadData()
    {
        food = dao.loadAllFood()
    }

    fun reloadData(location: Location) {
        food = dao.loadFoodByLocation(location.name)
    }

    fun reloadData(location: Location, category: FoodCategory) {
        food = dao.loadFood(category.name, location.name)
    }

    fun insert(food: Food) = viewModelScope.launch {
        dao.insertFood(food)
    }
}