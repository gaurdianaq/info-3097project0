package com.gaurdianaq.pantryorganizer.database

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import java.io.Serializable
import java.sql.Date

@Entity
data class Location(
    @PrimaryKey val name: String,
    val catname: String
) : Serializable

@Entity
data class FoodCategory(
    @PrimaryKey val name: String,
    val description: String
) : Serializable

@Entity(foreignKeys = [
    ForeignKey(
        entity = FoodCategory::class,
        parentColumns = arrayOf("name"),
        childColumns = arrayOf("foodcat"),
        onDelete = CASCADE
        ),
    ForeignKey(
        entity = Location::class,
        parentColumns = arrayOf("name"),
        childColumns = arrayOf("locname"),
        onDelete = CASCADE
    )
    ]
)
data class Food(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(index = true) val foodcat: String,
    @ColumnInfo(index = true) val locname: String,
    val name: String,
    val qty: Int
) : Serializable