package com.gaurdianaq.pantryorganizer.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*
import java.sql.Date

enum class QueryFlag { LOCATION, CATEGORY, LOCCAT }

class Converters {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }
}


@Dao
interface PantryDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertLocation(location: Location)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertFoodCategory(foodCategory: FoodCategory)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertFood(food: Food)

    @Update
    suspend fun updateLocation(location: Location)

    @Update
    suspend fun updateFoodCategory(foodCategory: FoodCategory)

    @Update
    suspend fun updateFood(food: Food)

    @Delete
    suspend fun deleteLocation(vararg locations: Location)

    @Delete
    suspend fun deleteFoodCategory(vararg foodCategories: FoodCategory)

    @Delete
    suspend fun deleteFood(vararg food: Food)

    @Query("SELECT * FROM food WHERE locname = :locname")
    fun loadFoodByLocation(locname: String) : LiveData<List<Food>>

    @Query("SELECT * FROM food WHERE foodcat = :foodcat")
    fun loadFoodByFoodCategory(foodcat: String) : LiveData<List<Food>>

    @Query("SELECT * FROM food WHERE foodcat = :foodcat AND locname = :locname")
    fun loadFood(foodcat: String, locname: String) : LiveData<List<Food>>

    @Query("SELECT * FROM food")
    fun loadAllFood() : LiveData<List<Food>>

    @Query("SELECT * FROM location")
    fun loadLocation() : LiveData<List<Location>>

    @Query("SELECT * FROM foodcategory")
    fun loadFoodCategory() : LiveData<List<FoodCategory>>
}

/*
@Database(entities = [Food::class, FoodCategory::class, Location::class], version = 1)
@TypeConverters(Converters::class)
abstract class PantryDatabase : RoomDatabase() {
    abstract fun dao(): PantryDAO
}*/


@Database(entities = [Food::class, FoodCategory::class, Location::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class PantryDatabase : RoomDatabase() {
    abstract fun dao(): PantryDAO

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: PantryDatabase? = null

        fun getDatabase(context: Context): PantryDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PantryDatabase::class.java,
                    "pantry_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
