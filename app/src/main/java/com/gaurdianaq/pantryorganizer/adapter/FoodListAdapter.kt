package com.gaurdianaq.pantryorganizer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.gaurdianaq.pantryorganizer.R
import com.gaurdianaq.pantryorganizer.database.Food
import com.gaurdianaq.pantryorganizer.database.PantryDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FoodListAdapter internal constructor(context: Context) : RecyclerView.Adapter<FoodListAdapter.FoodViewHolder>()
{
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var food = emptyList<Food>()
    private val context = context

    inner class FoodViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView)
    {
        val foodNameView: TextView = itemView.findViewById(R.id.foodNametxt)
        val foodQtyView: TextView = itemView.findViewById(R.id.Qtytxt)
        val cardView: CardView = itemView.findViewById(R.id.foodCard)
        val dataDAO = PantryDatabase.getDatabase(context).dao() //not a fan of this but currently I can't think of another way to do it and don't want to spend hours doing a rewrite

        init {
            cardView.setOnLongClickListener(fun (view:View):Boolean{
                val food = food.find { food -> food.name == foodNameView.text.toString() }
                GlobalScope.launch {
                    dataDAO.deleteFood(food!!)
                }
                return true
            })
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        val itemView = inflater.inflate(R.layout.foodview_item, parent, false)
        return FoodViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        val current = food[position]
        holder.foodNameView.text = current.name
        holder.foodQtyView.text = current.qty.toString()
    }

    override fun getItemCount() = food.size

    internal fun setFood(food: List<Food>)
    {
        this.food = food
        notifyDataSetChanged()
    }
}