package com.gaurdianaq.pantryorganizer.adapter

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.gaurdianaq.pantryorganizer.database.FoodCategory
import com.gaurdianaq.pantryorganizer.database.Location

class LocationSpinnerAdapter internal constructor(context: Context, resourceId: Int, locations:List<Location>) : ArrayAdapter<Location>(context, resourceId, locations)
{
    val locations:List<Location> = locations
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label: TextView = super.getView(position, convertView, parent) as TextView
        label.setTextColor(Color.BLACK)
        label.text = locations[position].name

        return label
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label: TextView = super.getView(position, convertView, parent) as TextView
        label.setTextColor(Color.BLACK)
        label.text = locations[position].name

        return label
    }

}

class FoodCategorySpinnerAdapter internal constructor(context: Context, resourceId: Int, categories:List<FoodCategory>) : ArrayAdapter<FoodCategory>(context, resourceId, categories)
{
    val categories:List<FoodCategory> = categories

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label: TextView = super.getView(position, convertView, parent) as TextView
        label.setTextColor(Color.BLACK)
        label.text = categories[position].name

        return label
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val label: TextView = super.getView(position, convertView, parent) as TextView
        label.setTextColor(Color.BLACK)
        label.text = categories[position].name

        return label
    }
}