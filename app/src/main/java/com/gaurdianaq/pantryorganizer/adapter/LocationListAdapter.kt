package com.gaurdianaq.pantryorganizer.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.gaurdianaq.pantryorganizer.R
import com.gaurdianaq.pantryorganizer.RecycleFoodbyLoc
import com.gaurdianaq.pantryorganizer.database.Location
import com.gaurdianaq.pantryorganizer.database.PantryDatabase
import com.gaurdianaq.pantryorganizer.locationFlag
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LocationListAdapter internal constructor(context: Context) : RecyclerView.Adapter<LocationListAdapter.LocationViewHolder>()
{
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var locations = emptyList<Location>()
    private val context = context

    inner class LocationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val locationItemViewName: TextView = itemView.findViewById(R.id.locationName)
        val locationItemViewImg: ImageView = itemView.findViewById(R.id.locationIcon)
        val dataDAO = PantryDatabase.getDatabase(context).dao() //not a fan of this but currently I can't think of another way to do it and don't want to spend hours doing a rewrite
        val cardView: CardView = itemView.findViewById(R.id.locationCard)
        init {
            cardView.setOnClickListener(fun (view:View){
                val foodByLocIntent = Intent(context, RecycleFoodbyLoc::class.java)
                val location = locations.find { location -> location.name == locationItemViewName.text.toString() }
                foodByLocIntent.putExtra(locationFlag, location)
                context.startActivity(foodByLocIntent)
            })

            cardView.setOnLongClickListener(fun (view:View): Boolean {
                val location = locations.find { location -> location.name == locationItemViewName.text.toString() }
                GlobalScope.launch {
                    dataDAO.deleteLocation(location!!)
                }

                return true
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val itemView = inflater.inflate(R.layout.locationview_item, parent, false)
        return LocationViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        val current = locations[position]
        holder.locationItemViewName.text = current.name
        when (current.catname)
        {
            "RoomTemp" -> holder.locationItemViewImg.setImageDrawable(context.getDrawable(R.drawable.cupboard))
            "Cold" -> holder.locationItemViewImg.setImageDrawable(context.getDrawable(R.drawable.fridge))
            "Frozen" -> holder.locationItemViewImg.setImageDrawable(context.getDrawable(R.drawable.freezer))
        }
    }

    override fun getItemCount() = locations.size

    internal fun setLocations(locations: List<Location>)
    {
        this.locations = locations
        notifyDataSetChanged()
    }
}