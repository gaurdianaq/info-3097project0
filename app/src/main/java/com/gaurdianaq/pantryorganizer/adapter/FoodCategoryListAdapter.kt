package com.gaurdianaq.pantryorganizer.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.gaurdianaq.pantryorganizer.*
import com.gaurdianaq.pantryorganizer.database.Food
import com.gaurdianaq.pantryorganizer.database.FoodCategory
import com.gaurdianaq.pantryorganizer.database.PantryDatabase
import com.gaurdianaq.pantryorganizer.viewmodel.CategoryViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FoodCategoryListAdapter internal constructor(context: Context) : RecyclerView.Adapter<FoodCategoryListAdapter.FoodCategoryHolder>()
{
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var foodCategory = emptyList<FoodCategory>()
    private val context = context

    inner class FoodCategoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val categoryName: TextView = itemView.findViewById(R.id.categoryNametxt)
        val categoryDesc: TextView = itemView.findViewById(R.id.categoryDescriptiontxt)
        val cardView: CardView = itemView.findViewById(R.id.categoryCard)
        val dataDAO = PantryDatabase.getDatabase(context).dao() //not a fan of this but currently I can't think of another way to do it and don't want to spend hours doing a rewrite
        init {
            cardView.setOnClickListener(fun (view:View){
                val foodByCatIntent = Intent(context, RecycleFoodbyCat::class.java)
                val category = foodCategory.find { category -> category.name == categoryName.text.toString() }
                foodByCatIntent.putExtra(categoryFlag, category)
                context.startActivity(foodByCatIntent)
            })

            cardView.setOnLongClickListener(fun (view:View): Boolean {
                val category = foodCategory.find { category -> category.name == categoryName.text.toString() }
                GlobalScope.launch {
                    dataDAO.deleteFoodCategory(category!!)
                }

                return true
            })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodCategoryHolder {
        val itemView = inflater.inflate(R.layout.categoryview_item, parent, false)
        return FoodCategoryHolder(itemView)
    }

    override fun onBindViewHolder(holder: FoodCategoryHolder, position: Int) {
        val current = foodCategory[position]
        holder.categoryName.text = current.name
        holder.categoryDesc.text = current.description
    }

    override fun getItemCount() = foodCategory.size

    internal fun setFoodCategory(foodCategory: List<FoodCategory>)
    {
        this.foodCategory = foodCategory
        notifyDataSetChanged()
    }
}