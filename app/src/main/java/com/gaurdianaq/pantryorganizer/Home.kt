package com.gaurdianaq.pantryorganizer

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gaurdianaq.pantryorganizer.adapter.LocationListAdapter
import com.gaurdianaq.pantryorganizer.viewmodel.LocationViewModel

import kotlinx.android.synthetic.main.activity_home.*
import java.lang.Exception

class Home : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(homeToolBar)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.about -> {
                val myIntent = Intent(this, About::class.java)
                startActivity(myIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun viewByCat(view: View) {
        try {
            val createCatIntent = Intent(this, RecycleCategories::class.java)
            startActivity(createCatIntent)
        }
        catch (ex:java.lang.Exception)
        {
            System.out.println("Oh dear")
        }
    }
    fun viewByLoc(view: View) {
        val createLocIntent = Intent(this, RecycleLocations::class.java)
        startActivity(createLocIntent)
    }

}
