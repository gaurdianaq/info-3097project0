package com.gaurdianaq.pantryorganizer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gaurdianaq.pantryorganizer.adapter.LocationListAdapter
import com.gaurdianaq.pantryorganizer.database.Location
import com.gaurdianaq.pantryorganizer.viewmodel.LocationViewModel

import kotlinx.android.synthetic.main.activity_recycle_locations.*

class RecycleLocations : AppCompatActivity() {

    private lateinit var locationViewModel: LocationViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycle_locations)
        setSupportActionBar(locationsToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val recyclerView = findViewById<RecyclerView>(R.id.locationList)
        val adapter = LocationListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        locationViewModel = ViewModelProvider(this).get(LocationViewModel::class.java)
        locationViewModel.locations.observe(this, Observer { location ->
            location?.let { adapter.setLocations(it) }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_locations, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.createLoc -> {
                val myIntent = Intent(this, CreateLoc::class.java)
                startActivityForResult(myIntent, insertRequestCode)
                true
            }
            R.id.about -> {
                val myIntent = Intent(this, About::class.java)
                startActivity(myIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == insertRequestCode && resultCode == Activity.RESULT_OK)
        {
            lateinit var location:Location
            data?.extras?.let {
                location = it.getSerializable(locationFlag) as Location
                locationViewModel.insert(location)
            }
            val okToast = Toast.makeText(this, "Added ${location.name}!", Toast.LENGTH_LONG)
            okToast.show()
        }
        else if(requestCode == insertRequestCode && resultCode == Activity.RESULT_CANCELED)
        {
            val cancelToast = Toast.makeText(this, "Cancelled location add!", Toast.LENGTH_LONG)
            cancelToast.show()
        }
    }

}
