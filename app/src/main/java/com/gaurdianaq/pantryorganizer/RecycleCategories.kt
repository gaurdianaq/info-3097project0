package com.gaurdianaq.pantryorganizer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gaurdianaq.pantryorganizer.adapter.FoodCategoryListAdapter
import com.gaurdianaq.pantryorganizer.database.FoodCategory
import com.gaurdianaq.pantryorganizer.viewmodel.CategoryViewModel

import kotlinx.android.synthetic.main.activity_recycle_categories.*

class RecycleCategories : AppCompatActivity() {
    private lateinit var foodCategoryViewModel: CategoryViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycle_categories)
        setSupportActionBar(categoriesToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val recyclerView = findViewById<RecyclerView>(R.id.categoriesList)
        val adapter = FoodCategoryListAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        foodCategoryViewModel = ViewModelProvider(this).get(CategoryViewModel::class.java)
        foodCategoryViewModel.categories.observe(this, Observer { category ->
            category?.let { adapter.setFoodCategory(it) }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_categories, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.createCat -> {
                val myIntent = Intent(this, CreateCat::class.java)
                startActivityForResult(myIntent, insertRequestCode)
                true
            }
            R.id.about -> {
                val myIntent = Intent(this, About::class.java)
                startActivity(myIntent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == insertRequestCode && resultCode == Activity.RESULT_OK)
        {
            lateinit var category:FoodCategory
            data?.extras?.let {
                category = it.getSerializable(categoryFlag) as FoodCategory
                foodCategoryViewModel.insert(category)
            }
            val okToast = Toast.makeText(this, "Added ${category.name}!", Toast.LENGTH_LONG)
            okToast.show()
        }
        else if(requestCode == insertRequestCode && resultCode == Activity.RESULT_CANCELED)
        {
            val cancelToast = Toast.makeText(this, "Cancelled category add!", Toast.LENGTH_LONG)
            cancelToast.show()
        }
    }

}
