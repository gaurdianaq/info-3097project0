package com.gaurdianaq.pantryorganizer

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import com.gaurdianaq.pantryorganizer.adapter.LocationSpinnerAdapter
import com.gaurdianaq.pantryorganizer.database.*
import kotlinx.android.synthetic.main.activity_create_foodbycat.*

class CreateFoodByCat : AppCompatActivity() {
    private lateinit var editName: EditText
    private lateinit var editQty: EditText
    private lateinit var editFoodLoc: Spinner
    private lateinit var locations: Array<Location>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_foodbycat)
        setSupportActionBar(createFoodToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        locations = intent.extras?.getSerializable(locationFlag) as Array<Location>
        editName = findViewById(R.id.editFoodName)
        editQty = findViewById(R.id.editQty)
        editFoodLoc = findViewById(R.id.locpicker)
        val locationsAdapter = LocationSpinnerAdapter(this, android.R.layout.simple_spinner_item, locations.toList())
        locationsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        editFoodLoc.adapter = locationsAdapter
    }

    fun cancel(view: View) {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    fun submit(view: View) {
        if (TextUtils.isEmpty(editName.text) && (TextUtils.isEmpty(editQty.text)))
        {
            val warningToast = Toast.makeText(this, "Text fields can't be empty!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else if (TextUtils.isEmpty(editName.text))
        {
            val warningToast = Toast.makeText(this, "Name field is empty! Not allowed!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else if (TextUtils.isEmpty(editQty.text))
        {
            val warningToast = Toast.makeText(this, "Qty field is empty! Not allowed!", Toast.LENGTH_LONG)
            warningToast.show()
        }
        else
        {
            val replyIntent = Intent()
            val category = intent.extras?.getSerializable(categoryFlag) as FoodCategory
            val food = Food(0, category.name, (editFoodLoc.selectedItem as Location).name, editName.text.toString(), editQty.text.toString().toInt())
            replyIntent.putExtra(foodFlag, food)
            setResult(Activity.RESULT_OK, replyIntent)
            finish()
        }
    }

}
